import mouse
import keyboard
import time
import re
import pyperclip
import datetime
from random import randrange
import subprocess

try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # ver. < 3.0

# Changing the Current Working Directory
import os
#to be changed based on ini file path
os.chdir('D:\')

# READING INI FILE
print('READING INI FILE...')
config = ConfigParser()
config.read('connectMEgitea.ini')
# reading values from all sections
# TO BE ADJUSTED BASED ON FRIENDS AND FAMILY (this part can be added as part of the program later)
# friends
julia = float(config.get('friends', 'julia'))
omar = float(config.get('friends', 'omar'))
leila = float(config.get('friends', 'leila'))
eddy = float(config.get('friends', 'eddy'))
# family
mom = float(config.get('family', 'mom'))
dad = float(config.get('family', 'dad'))
sis = float(config.get('family', 'sis'))
bro = float(config.get('family', 'bro'))

print('\n\n\nCONNECT ME BOT\n')

cc = 1
# link of folder containing bot and ini file
linka = "D:\"
# link of ini file
linkb = "D:\connectMEgitea.ini"
# link of bot
linkc = "D:\connectME GITEA.py"

print('CONNECT ME BOT')
print(' ')

while cc == 1:
 print('connect > CALL OR MESSAGE FRIEND')
 print('set > CONFIGURE connectME')
 print('res > RESTART connectME')
 print(' ')
 # READING INI FILE
 print('READING INI FILE...')
 config = ConfigParser()
 config.read('connectMEgitea.ini')
 #reading values from all sections
 # friends
 julia = float(config.get('friends', 'julia'))
 omar = float(config.get('friends', 'omar'))
 leila = float(config.get('friends', 'leila'))
 eddy = float(config.get('friends', 'eddy'))
 # family
 mom = float(config.get('family', 'mom'))
 dad = float(config.get('family', 'dad'))
 sis = float(config.get('family', 'sis'))
 bro = float(config.get('family', 'bro'))
 # CALCULATING AVERAGES
 list_friends = [julia,omar,leila,eddy]
 list_family = [mom,dad,sis,bro]
 avg_friends = (sum(list_friends))/4
 avg_family = (sum(list_family))/4
 print("Average friends: "+str(avg_friends))
 print("Average family: "+str(avg_family))
 time.sleep(1)
 # INPUT COMMAND
 begins = input('\nPLEASE INPUT COMMAND ')
 # SET
 if begins == 'set':
  subprocess.Popen(['C:\\WINDOWS\\system32\\notepad.exe','D:\\connectME GITEA.py'])
 if begins == 'res':
  # RESTART
  print('RESTARTING...')
  subprocess.call(['C:\\WINDOWS\\py.exe','D:\\connectME GITEA.py'])
 # CONNECT
 if begins == 'connect':
  print('CHOOSING...')
  min_contact = min(avg_friends,avg_family)
  time.sleep(1)
  print("min_contact: "+str(min_contact))
  time.sleep(1)
  pw = 1
  avo = 1
  while pw == 1:
   # FRIENDS
   if avg_friends <= min_contact:
    while avo == 1:
     ava = randrange(1,5)
     print("choosing randomly from friends...")
     print(str(ava))
     if ava == 1:
      if leila <= avg_friends:
       avo = 2
       subprocess.Popen(['C:\\Users\\User\\AppData\\Local\\WhatsApp\\WhatsApp.exe'])
       time.sleep(12)
       mouse.move(1190,114)
       time.sleep(0.5)
       mouse.click(button='left')
       pyperclip.copy("leila")
       time.sleep(0.5)
       keyboard.send("ctrl+v,enter")
       time.sleep(1.5)
       pyperclip.copy("Hello Leila! How are you?")
       time.sleep(0.5)
       keyboard.send("ctrl+v")
       #the smaller the number below the more this person will be contacted
       leila += 1
       config.set('friends', 'leila', str(leila))
       with open('connectMEgitea.ini', 'w') as configfile:
        config.write(configfile)
       break
     if ava == 2:
      if omar <= avg_friends:
       avo = 2
       subprocess.Popen(['C:\\Users\\User\\AppData\\Local\\WhatsApp\\WhatsApp.exe'])
       time.sleep(12)
       mouse.move(1190,114)
       time.sleep(0.5)
       mouse.click(button='left')
       pyperclip.copy("omar")
       time.sleep(0.5)
       keyboard.send("ctrl+v,enter")
       time.sleep(1.5)
       pyperclip.copy("Hello Omar! How are you?")
       time.sleep(0.5)
       keyboard.send("ctrl+v")
       #the smaller the number below the more this person will be contacted
       omar += 1
       config.set('friends', 'omar', str(omar))
       with open('connectMEgitea.ini', 'w') as configfile:
        config.write(configfile)
       break
     if ava == 3:
      if julia <= avg_friends:
       avo = 2
       subprocess.Popen(['C:\\Users\\User\\AppData\\Local\\WhatsApp\\WhatsApp.exe'])
       time.sleep(12)
       mouse.move(1190,114)
       time.sleep(0.5)
       mouse.click(button='left')
       pyperclip.copy("julia")
       time.sleep(0.5)
       keyboard.send("ctrl+v,enter")
       time.sleep(1.5)
       pyperclip.copy("Hello Julia! How are you?")
       time.sleep(0.5)
       keyboard.send("ctrl+v")
       #the smaller the number below the more this person will be contacted
       julia += 1
       config.set('friends', 'julia', str(julia))
       with open('connectMEgitea.ini', 'w') as configfile:
        config.write(configfile)
       break
     if ava == 4:
      if eddy <= avg_friends:
       avo = 2
       subprocess.Popen(['C:\\Users\\User\\AppData\\Local\\WhatsApp\\WhatsApp.exe'])
       time.sleep(12)
       mouse.move(1190,114)
       time.sleep(0.5)
       mouse.click(button='left')
       pyperclip.copy("eddy")
       time.sleep(0.5)
       keyboard.send("ctrl+v,enter")
       time.sleep(1.5)
       pyperclip.copy("Hello Eddy! How are you?")
       time.sleep(0.5)
       keyboard.send("ctrl+v")
       #the smaller the number below the more this person will be contacted
       eddy += 1
       config.set('friends', 'eddy', str(eddy))
       with open('connectMEgitea.ini', 'w') as configfile:
        config.write(configfile)
       break
    pw = 0
    break
   # FAMILY
   if avg_family <= min_contact:
    while avo == 1:
     ava = randrange(1,5)
     print("choosing randomly from family...")
     print(str(ava))
     if ava == 1:
      if mom <= avg_family:
       avo = 2
       subprocess.Popen(['C:\\Users\\User\\AppData\\Local\\WhatsApp\\WhatsApp.exe'])
       time.sleep(12)
       mouse.move(1190,114)
       time.sleep(0.5)
       mouse.click(button='left')
       pyperclip.copy("mom")
       time.sleep(0.5)
       keyboard.send("ctrl+v,enter")
       time.sleep(1.5)
       pyperclip.copy("Hi Mom! How are you?")
       time.sleep(0.5)
       keyboard.send("ctrl+v")
       #the smaller the number below the more this person will be contacted
       mom += 1
       config.set('family', 'mom', str(mom))
       with open('connectMEgitea.ini', 'w') as configfile:
        config.write(configfile)
       break
     if ava == 2:
      if dad <= avg_family:
       avo = 2
       subprocess.Popen(['C:\\Users\\User\\AppData\\Local\\WhatsApp\\WhatsApp.exe'])
       time.sleep(12)
       mouse.move(1190,114)
       time.sleep(0.5)
       mouse.click(button='left')
       pyperclip.copy("dad")
       time.sleep(0.5)
       keyboard.send("ctrl+v,enter")
       time.sleep(1.5)
       pyperclip.copy("Hi Dad! How are you?")
       time.sleep(0.5)
       keyboard.send("ctrl+v")
       #the smaller the number below the more this person will be contacted
       dad += 1
       config.set('family', 'dad', str(dad))
       with open('connectMEgitea.ini', 'w') as configfile:
        config.write(configfile)
       break
     if ava == 3:
      if sis <= avg_family:
       avo = 2
       subprocess.Popen(['C:\\Users\\User\\AppData\\Local\\WhatsApp\\WhatsApp.exe'])
       time.sleep(12)
       mouse.move(1190,114)
       time.sleep(0.5)
       mouse.click(button='left')
       pyperclip.copy("sis")
       time.sleep(0.5)
       keyboard.send("ctrl+v,enter")
       time.sleep(1.5)
       pyperclip.copy("Hi Sis! How are you?")
       time.sleep(0.5)
       keyboard.send("ctrl+v")
       #the smaller the number below the more this person will be contacted
       sis += 1
       config.set('family', 'sis', str(sis))
       with open('connectMEgitea.ini', 'w') as configfile:
        config.write(configfile)
       break
     if ava == 4:
      if bro <= avg_family:
       avo = 2
       subprocess.Popen(['C:\\Users\\User\\AppData\\Local\\WhatsApp\\WhatsApp.exe'])
       time.sleep(12)
       mouse.move(1190,114)
       time.sleep(0.5)
       mouse.click(button='left')
       pyperclip.copy("bro")
       time.sleep(0.5)
       keyboard.send("ctrl+v,enter")
       time.sleep(1.5)
       pyperclip.copy("Hi Bro! How are you?")
       time.sleep(0.5)
       keyboard.send("ctrl+v")
       #the smaller the number below the more this person will be contacted
       bro += 1
       config.set('family', 'bro', str(bro))
       with open('connectMEgitea.ini', 'w') as configfile:
        config.write(configfile)
       break
    pw = 0
    break